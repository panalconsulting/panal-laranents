<?php
/**
 * This configuration will be read and overlaod on top of the
 * default configuration. Command line arguments will be applied
 * after this file is read.
 */
return [
    'theme' => 'admin-lte-3',
];
