<?php

namespace Panalsoft\Laranents\Console\Commands;

use Illuminate\Console\Command;

class CreateLinkAssets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'panal-laranents:link-assets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea un link simbólico a los assets del paquete de componentes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(base_path('resources/vendor/laranents'))) {
            return $this->error('El link symbólico ya existes');
        }

        $this->laravel->make('files')->link(
            '../vendor/panalsoft/laranents/resources/' . config('panal-laranents.theme') . '/assets', base_path('resources/laranents')
        );

        $this->info('Link creado en [resources/laranents].');
    }
}
