<?php

namespace Panalsoft\Laranents;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Panalsoft\Laranents\LaranentsClass
 */
class LaranentsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'panal-laranents';
    }
}
