<?php

namespace Panalsoft\Laranents;

use Illuminate\Support\ServiceProvider;

class LaranentsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/' . config('panal-laranents.theme') . '/views', 'panal-laranents');

        if ($this->app->runningInConsole()) {

            $this->commands([
                'Panalsoft\Laranents\Console\Commands\CreateLinkAssets'
            ]);

            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('panal-laranents.php'),
            ], 'config');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'panal-laranents');
    }
}
