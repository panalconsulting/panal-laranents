# panal-laranents

Componentes para backends para laravel

## Installation

Agregar al composer json:

```js
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/panalconsulting/panal-laranents.git"
    }
]
```

Instalar con composer:

```bash
# develop
composer require panalsoft/laranents:dev-master
# fijar versión con TAG
composer require panalsoft/laranents:dev-master#1.0.0
```

Crear link a los assets:

```bash
php artisan panal-laranents:link-assets
```

Opcionalmente se puede publicar el archivo de configuración:

```bash
php artisan vendor:publish --tag=config --provider='Panalsoft\Laranents\LaranentsServiceProvider'
```
## Usage

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
