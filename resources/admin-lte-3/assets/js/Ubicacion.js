import React from 'react';
import ReactDOM from 'react-dom';

export default class Ubicacion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      marker: {
        lat: props.lat,
        lng: props.lng,
      },
      direccion: props.direccion,
    };
    this.handleDirChange = this.handleDirChange.bind(this);
    this.updateLatLng = this.updateLatLng.bind(this);
    this.addMarker = this.addMarker.bind(this);
    this.setLatLngState = this.setLatLngState.bind(this);
    this.googleMapsClient = require('@google/maps').createClient({
      key: props.gmapsApiKey,
      Promise: Promise
    });
    this.map = null;
    this.marker = null;
  }

  componentDidMount() {
    this.map = new google.maps.Map(document.getElementById('ubicacion-map'), {
      zoom: 14,
      fullscreenControl: true,
      scrollwheel: false,
      center: new google.maps.LatLng(-54.8063155, -68.3060354)
    });
    const {lat, lng} = this.state.marker;
    if (lat) {
      this.addMarker();
      this.updateLatLng();
    }
    this.map.addListener('click', this.setLatLngState);
  }

  addMarker() {
    const {lat, lng} = this.state.marker;
    try {
      const latLng = new google.maps.LatLng(lat, lng);
      if (this.marker) {
        this.marker.setMap(null);
      }
      this.marker = new google.maps.Marker({
        position: latLng,
        map: this.map,
        draggable: true,
      });
      // this.map.setCenter(latLng); // setCenter takes a LatLng object
      this.map.panTo(this.marker.getPosition());
      this.map.setZoom(17);
      this.marker.addListener('dragend', this.setLatLngState)
    } catch (e) {
      console.log(e);
    }
  }

  updateLatLng() {
    if (this.state.direccion) {
      this.googleMapsClient
      .geocode({address: this.state.direccion + ' Ushuaia, Tierra del Fuego, Argentina'})
      .asPromise()
      .then((response) => {
        this.setState({
          marker: {
            lat: response.json.results["0"].geometry.location.lat,
            lng: response.json.results["0"].geometry.location.lng
          }
        }, this.addMarker)
      })
      .catch((err) => {
        console.log(err);
      });
    }
  };

  handleDirChange(event){
    this.setState({direccion: event.target.value});
  }

  setLatLngState(e){
    this.setState({
      marker: {
        lat: e.latLng.lat(),
        lng: e.latLng.lng()
      }
    }, this.addMarker)
  }

  render() {
    const {direccion} = this.state;
    const {lat, lng} = this.state.marker;
    return (
      <div>
        <div className="form-group">
          <label htmlFor="direccion" className="required">Dirección</label>
          <input
            className="form-control" required type="text" name="direccion" id="direccion"
            value={direccion}
            onBlur={this.updateLatLng}
            onChange={this.handleDirChange}
            tabIndex="3"
          />
        </div>
        <div className="form-group">
          <label htmlFor="lat" className="required">
            Por favor, confirmá la dirección en el mapa haciendo click en la ubicación exacta
          </label>
          <input value={lat} type="hidden" className="lat" name="lat" required />
          <input value={lng} type="hidden" className="lng" name="lng" required />
          <div id="ubicacion-map" style={{width: '100%', height: '400px'}}></div>
        </div>
      </div>
    );
  }
}

if (document.getElementById('ubicacion')) {
  let element = document.getElementById('ubicacion');
  ReactDOM.render(<Ubicacion
    lat={element.dataset.lat}
    lng={element.dataset.lng}
    direccion={element.dataset.direccion}
    gmapsApiKey={element.dataset.gmapsapikey}
    />, element);
  }
