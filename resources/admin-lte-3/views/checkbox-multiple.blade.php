{{-- @component('components.checkbox-multiple', [
    'label' => '',
    'name' => '',
    'collection' => collect([
        (object)[
            'label' => 'Nacional',
            'value' => 1,
        ],
        (object)[
            'label' => 'Provincial',
            'value' => 0,
        ],
    ]),
    'selectedItems' => ,
])
@endcomponent --}}

<div class="form-group clearfix">
    <div class="d-inline">
        <label>{{$label}}</label>
    </div>
    @foreach ($collection as $item)
        @component('components.checkbox', [
            'style' => isset($style) ? $style : 'primary',
            'name' => $name, 
            'label' => $item->label, 
            'checked' => $selectedItems->contains($item->value),
        ])
        @endcomponent('components.checkbox')
    @endforeach
</div>