{{-- @component('components.select2', [
    'inputName' => 'role', 
    'inputClass' => '', 
    'inputLabel' => 'Roles', 
    'required' => true,
    'multiple' => false,
    'placeholder' => 'Seleccionar una opción',
    'collection' => $roles,
    'itemLabel' => 'name',
    'itemValue' => 'id',
    'itemSelected' => optional($user->roles->first())->id,
    'attrs' => ''
])
@endcomponent('components.select2') --}}

<div class="form-group">
    <label 
        class="{{(isset($required) && $required) ? 'required' : ''}}" 
        for="{{$inputName}}"
    >
        {{$inputLabel}}
    </label>
    <select 
        class="{{$inputClass or ''}} form-control custom-select w-100 select2" 
        name="{{$inputName}}{{(isset($multiple) && $multiple) ? '[]' : ''}}" 
        id="{{$inputName}}"
        {{(isset($placeholder) && (isset($multiple) && $multiple)) ? "data-placeholder={$placeholder}" : ''}}
        {{(isset($required) && $required) ? 'required' : ''}}
        {{(isset($multiple) && $multiple) ? 'multiple' : ''}}
        {{isset($attrs) ? $attrs : ''}}   
    >
        @if (isset($options))
            {{$options}}
        @else
            @if (isset($placeholder) && (!isset($multiple) || !$multiple))
                <option selected value="" @if(isset($required) && $required) disabled @endif>{{$placeholder}}</option>
            @endif
            @foreach ($collection as $item)
                <option 
                    @if (isset($itemValue))
                        @if ($itemSelected == $item->$itemValue) selected @endif 
                        value="{{$item->$itemValue}}"
                    @else
                        @if ($itemSelected == $item) selected @endif 
                        value="{{$item}}"
                    @endif
                >
                    @if (isset($itemLabel))
                        {{$item->$itemLabel}}
                    @else
                        {{$item}}
                    @endif 
                </option>
            @endforeach
        @endif
    </select>
</div>


{{--  para multiple agregar que pueden ser varios seleccionados y chequear los [] despues del nombre--}}
