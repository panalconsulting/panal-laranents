{{-- @component('components.textarea-labeled', [
  'inputName' => 'descripcion', 
  'inputLabel' => 'Descripción', 
  'inputValue' => old('descripcion', $intervencion->descripcion),
  'required' => true,
  'labelClass' => '',
  'inputAttrs' => '',
])
@endcomponent('components.textarea-labeled') --}}

<div class="form-group">
  <label class="{{(isset($required) && $required) ? 'required' : ''}} {{$labelClass or ''}}" for="{{$inputName}}">
    {{$inputLabel}}
  </label>
  <textarea 
    {!! $inputAttrs or '' !!} 
    {{(isset($required) && $required) ? 'required' : ''}}
    class="form-control {{ $errors->has($inputName) ? 'is-invalid' : '' }}" 
    rows="3"
    id="{{$inputName}}" 
    name="{{$inputName}}" 
  >{{$inputValue or ''}}</textarea>
  {!! $errors->first($inputName, '<div class="invalid-feedback">:message</div>') !!}
</div>
