{{-- @component('components.radio', [
	'name' => 'jefe_familia', 
	'label' => '¿Es jefe de familia?', 
	'checked' => old('jefe_familia', $caso->jefe_familia),
])
@endcomponent('components.radio') --}}

<div class="icheck-{{$style or 'primary'}} d-inline">    
	<input 
		id="{{$id}}"
		name="{{$name}}" 
		type="radio" 
		value="{{$value}}"
		{{isset($checked) && $checked ? 'checked' : ''}}
	/> 
	<label for="{{$id}}">{{$label}}</label>
</div>