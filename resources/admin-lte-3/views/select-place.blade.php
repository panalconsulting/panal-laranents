{{-- PARAMETROS --}}
{{-- inputName,
    inputLabel,
    required,
    collection,
    itemValue,
    itemLabel,
    itemSelected 
--}}
{{-- VALOS PRE-EXISTENTES--}}
{{-- 
    env = 'GOOGLE_MAPS_KEY'
    section llamada "before-body-close" para insertar js
--}}

<div class="form-group">
    <label 
        class="{{(isset($required) && $required) ? 'required' : ''}}" 
        for="{{$inputName}}"
    >
        {{$inputLabel}}
    </label>
    <select 
        class="{{$inputClass or ''}} form-control _custom-select w-100 _select2" 
        name="{{$inputName}}{{(isset($multiple) && $multiple) ? '[]' : ''}}" 
        id="{{$inputName}}"
        {{(isset($placeholder) && (isset($multiple) && $multiple)) ? "data-placeholder={$placeholder}" : ''}}
        {{(isset($required) && $required) ? 'required' : ''}}
        {{(isset($multiple) && $multiple) ? 'multiple' : ''}}
        {{isset($attrs) ? $attrs : ''}}
    >
        @if (isset($options))
            {{$options}}
        @else
            @if (isset($placeholder) && (!isset($multiple) || !$multiple))
                <option selected value="" disabled>{{$placeholder}}</option>
            @endif
            @if ($itemSelected)
            
                <option selected value="{{$itemSelected}}">{{$itemSelected}}</option>
            @endif
            {{-- @foreach ($collection as $item)
                <option 
                    @if (isset($itemValue))
                        @if ($itemSelected == $item->$itemValue) selected @endif 
                        value="{{$item->$itemValue}}"
                    @else
                        @if ($itemSelected == $item) selected @endif 
                        value="{{$item}}"
                    @endif
                >
                    @if (isset($itemLabel))
                        {{$item->$itemLabel}}
                    @else
                        {{$item}}
                    @endif 
                </option>
            @endforeach --}}
        @endif
    </select>
</div>

@push('before-body-close')
    <script>
        $('#{{$inputName}}').select2({
            placeholder: 'Ingrese como minimo 3 caractéres',
            language: 'es',
            ajax: {
                url: "https://maps.googleapis.com/maps/api/place/autocomplete/json",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        input: params.term,
                        key: "{{env('GOOGLE_MAPS_KEY')}}",
                        sessiontoken: 1234567890,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data.predictions.map(function(item){
                            item.id = item.description;
                            item.text = item.description;
                            return item;
                        })
                    };
                },
                cache: true
            },
            minimumInputLength: 3,
            templateResult: function(state) {
                if (!state.id) {
                    return state.text;
                }
                var $state = $(
                    '<span>' + state.description + '</span>'
                );
                return $state;
            },
            templateSelection: function(data) {
                return data.text;
            }
        });
    </script>
@endpush