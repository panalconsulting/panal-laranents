{{-- @component('components.select2-multiple', [
    'inputName' => 'role', 
    'inputClass' => '', 
    'inputLabel' => 'Roles', 
    'required' => true,
    'placeholder' => 'Seleccionar una opción',
    'collection' => $roles,
    'itemLabel' => 'name',
    'itemValue' => 'id',
    'itemsSelected' => collect(),
    'attrs' => ''
])
@endcomponent('components.select2-multiple') --}}

<div class="form-group">
    <label 
        class="{{(isset($required) && $required) ? 'required' : ''}}" 
        for="{{$inputName}}"
    >
        {{$inputLabel}}
    </label>
    <select 
        class="{{$inputClass or ''}} form-control custom-select w-100 select2" 
        name="{{$inputName}}[]" 
        id="{{$inputName}}"
        multiple
        {{isset($placeholder) ? "data-placeholder={$placeholder}" : ''}}
        {{(isset($required) && $required) ? 'required' : ''}}
        {{isset($attrs) ? $attrs : ''}}   
    >
        @if (isset($options))
            {{$options}}
        @else
            @foreach ($collection as $item)
                <option 
                    @if (isset($itemValue))
                        @if ($itemsSelected->contains($item->$itemValue)) selected @endif 
                        value="{{$item->$itemValue}}"
                    @else
                        @if ($itemsSelected->contains($item)) selected @endif 
                        value="{{$item}}"
                    @endif
                >
                    @if (isset($itemLabel))
                        {{$item->$itemLabel}}
                    @else
                        {{$item}}
                    @endif 
                </option>
            @endforeach
        @endif
    </select>
</div>