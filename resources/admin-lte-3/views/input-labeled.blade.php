{{-- @component('components.input-labeled', [
    'inputName' => 'name',
    'inputLabel' => 'Nombre',
    'inputType' => 'text',
    'inputValue' => old('name', $user->name),
	'required' => true,
	'labelClass' => '',
	'inputAttrs' => '',
])
@endcomponent('components.input-labeled') --}}
<div class="form-group">
	<label class="{{(isset($required) && $required) ? 'required' : ''}} {{$labelClass or ''}}" for="{{$inputName}}">
		INPUT LABELED {{$inputLabel}}
	</label>
	<input
		{!! $inputAttrs or '' !!}
		{{(isset($required) && $required) ? 'required' : ''}}
		type="{{$inputType}}"
		class="form-control {{$labelClass or ''}} {{ $errors->has($inputName) ? 'is-invalid' : '' }}"
		id="{{$inputName}}"
		name="{{$inputName}}"
		value="{{$inputValue or ''}}"
	>
	{!! $errors->first($inputName, '<div class="invalid-feedback">:message</div>') !!}
</div>
