{{-- @component('components.checkbox', [
	'name' => 'jefe_familia', 
	'label' => '¿Es jefe de familia?', 
	'value' => old('jefe_familia', $caso->jefe_familia),
])
@endcomponent('components.checkbox') --}}

<div class="icheck-{{$style or 'primary'}}">    
	<input 
		id="{{$name}}"
		name="{{$name}}" 
		type="checkbox" 
		{{isset($checked) && $checked ? 'checked' : ''}}
	/> 
	<label for="{{$name}}">{{$label}}</label>
</div>