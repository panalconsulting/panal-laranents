{{-- @component('components.bs-custom-file-input', [
    'inputLabel' => 'Seleccionar archivos',
    'inputName' => 'documentos',
    'multiple' => true,
    'inputClass' => '',
    'attrs' => '',
    'required' => true,
    'files' => collect(),
])
@endcomponent --}}

{{-- <h5>{{$inputLabel}}</h5> --}}
<div class="custom-file">
    <input 
        type="file" 
        name="{{$inputName}}{{(isset($multiple) && $multiple) ? '[]' : ''}}" 
        class="{{$inputClass or ''}} form-control custom-file-input" 
        id="{{$inputName}}"
        lang="es"
        {{isset($attrs) ? $attrs : ''}}
        {{(isset($required) && $required) ? 'required' : ''}}
        {{(isset($multiple) && $multiple) ? 'multiple' : ''}}>
    <label 
        class="custom-file-label" 
        for="{{$inputName}}">
        @if (isset($files) && !$files->isEmpty())
            {{$files->implode('name', ', ')}}
        @else
            {{$inputLabel}}
        @endif
    </label>
</div>

@push('before-body-close')
    <script>
        $(document).ready(function () {
            bsCustomFileInput.init()
        })
    </script>
@endpush