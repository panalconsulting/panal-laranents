<div
    id="ubicacion"
    data-lat="{{$lat}}"
    data-lng="{{$lng}}"
    data-direccion="{{$direccion}}"
    data-gmapsApiKey="{{$gmapsApiKey}}"
    style="width: 100%; height: 300px;"
></div>

@push('before-body-close')
    <script src="{{ asset('js/Ubicacion.js') }}"></script>
@endpush
