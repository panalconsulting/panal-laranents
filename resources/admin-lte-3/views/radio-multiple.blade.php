{{-- @component('components.radio-multiple', [
    'label' => '',
    'name' => '',
    'collection' => collect([
        (object)[
            'label' => 'Si',
            'id' => 'si',
            'value' => true,
        ],
        (object)[
            'label' => 'No',
            'id' => 'no',
            'value' => false,
        ],
    ]),
    'selectedItem' => ,
])
@endcomponent --}}
<div class="form-group clearfix">
    <label>{{$label}}</label>
    <br>
    @foreach ($collection as $item)
        @component('components.radio', [
            'style' => isset($style) ? $style : 'primary',
            'name' => $name, 
            'label' => $item->label, 
            'value' => $item->value, 
            'id' => $item->id, 
            'checked' => $selectedItem === $item->value,
        ])
        @endcomponent('components.radio')
    @endforeach
</div>